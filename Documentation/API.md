# Documentation API

L'API utilise le framework Leaf en version 2.11.1 et PHP en version 7.4.3

Nous avons utilisé la documentation de LeafAPI : https://leafphp.dev/docs/leafapi/

# Controllers

Les controllers servent à appler les foncitons du model

Les controllers devront être nommé avec le nom d'une base de donnée au pluriel.

    [Nom de la base]Controller

Pour simplifié la création des controllers on utilisera la commande suivante

    php leaf g:controller [Nom de la base] -m --resource

Cette commande crée le controller et mets le [Nom de la base] au pluriel. Elle crée aussi le Model associé au controller.

Elle créer aussi les fonction de base d'un controller :

. index() : retourne toutes les ligne présent dans la Table du modèle associé

. store() : permet d'ajouter une ligne dans la Table du modèle associé

. show() : retourne une ligne dont la clé primmaire est spécifiée en entrée

. update() : met à jour une ligne don la clé primmaire est spécifiée en entrée

. destroy() : supprime une ressource dont la clé primmaire est spécifiée en entrée

Pensez bien à retirer les $id en paramètre, sinon vos fonctions ne pourront pas être appler à l'exterieur de l'API


# Models

Les models servent à aller chercher et modifier la Base De Données

Générer avec la commande spécifié dans la partie **Controllers**, le model aura pour nom le [Nom de la base] au singulier

    [Nom de la base]

On pourra spécifié des informations sur la Table ciblée, on devra au minimum avoir les informations suivante 

    /**
     * The table associated with the model.
     *
     * @var string
     */
    
    protected $table = 'Personnes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'mail';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */

    protected $keyType = 'string';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    
    public $incrementing = false;

Pour plus d'information se reporter à la documentation de Leaf : https://leafphp.dev/docs/mvc/models.html#leaf-model-conventions

# Routes

Dans le fichier **_app.php** on a les URL qui seront exposé afin d'appeler l'API

Elle devront prendre la forme suivante

    app()->get('/[Nom de la table]', '[Nom du controller]@[Nom de la fonction]');

[Nom de la table] : 

. Doit être en minuscule et au pluriel

Nom du Controller

. Doit être le même que celui du controller

Nom de la fonction

. Doit être le même que celle de la fonction du controller que vous voulez appeler

# .env

Le fichier .env reprends les informations principales de l'application. On y trouve le type de Laguage de base de données ainsi que les informations pour s'y connecter. 