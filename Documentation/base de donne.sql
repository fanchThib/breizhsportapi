-- --------------------------------------------------------
-- Hôte:                         breizhsport-database.cl0esekwy1x4.eu-west-3.rds.amazonaws.com
-- Version du serveur:           PostgreSQL 15.5 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 7.3.1 20180712 (Red Hat 7.3.1-12), 64-bit
-- SE du serveur:                
-- HeidiSQL Version:             12.6.0.6799
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Listage de la structure de table public. Panier
IF NOT EXISTS ;

-- Listage des données de la table public.Panier : 2 rows
/*!40000 ALTER TABLE "Panier" DISABLE KEYS */;
INSERT INTO "Panier" ("id_panier", "mail_personne") VALUES
	(1, 'berlivetallan@gmail.fr'),
	(3, 'richardjean@yahoo.com');
/*!40000 ALTER TABLE "Panier" ENABLE KEYS */;

-- Listage de la structure de table public. Personnes
IF NOT EXISTS ;

-- Listage des données de la table public.Personnes : 2 rows
/*!40000 ALTER TABLE "Personnes" DISABLE KEYS */;
INSERT INTO "Personnes" ("mail", "nom", "prenom", "password") VALUES
	('berlivetallan@gmail.fr', 'Berlivet', 'Allan', 'allanBerlivet'),
	('richardjean@yahoo.com', 'Richard', 'Jean', 'jeanRichard');
/*!40000 ALTER TABLE "Personnes" ENABLE KEYS */;

-- Listage de la structure de table public. Produits
IF NOT EXISTS ;

-- Listage des données de la table public.Produits : 3 rows
/*!40000 ALTER TABLE "Produits" DISABLE KEYS */;
INSERT INTO "Produits" ("id", "nom", "description", "stock", "prix") VALUES
	(1, 'Tenis', 'Une paire de tenis classique comme on en trouve partout', 50, 20),
	(2, 'Ballon', ' Pour jouer avec vos magnifiques tenis', 25, 10),
	(3, 'Jogging', ' Pour vous habiller', 100, 25);
/*!40000 ALTER TABLE "Produits" ENABLE KEYS */;

-- Listage de la structure de table public. Produit_dans_panier
IF NOT EXISTS ;

-- Listage des données de la table public.Produit_dans_panier : 3 rows
/*!40000 ALTER TABLE "Produit_dans_panier" DISABLE KEYS */;
INSERT INTO "Produit_dans_panier" ("id_panier", "id_produit", "quantite") VALUES
	(1, 1, 2),
	(1, 3, 1),
	(3, 2, 6);
/*!40000 ALTER TABLE "Produit_dans_panier" ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
