<?php

app()->get('/', function () {
    response()->json(['message' => 'Congrats!! You\'re on Leaf API']);
});

// Les routes pour les endpoints des Personnes

app()->get('/personnes', 'PersonnesController@index');

app()->get('/personnes/store', 'PersonnesController@store');

app()->get('/personnes/show', 'PersonnesController@show');

app()->get('/personnes/update', 'PersonnesController@update');

app()->get('/personnes/destroy', 'PersonnesController@destroy');

// Les routes pour les endpoints des Paniers

app()->get('/panier', 'PaniersController@index');

app()->get('/panier/store', 'PaniersController@store');

app()->get('/panier/show', 'PaniersController@show');

app()->get('/panier/update', 'PaniersController@update');

app()->get('/panier/destroy', 'PaniersController@destroy');

// Les routes pour les endpoints des Produits

app()->get('/produit', 'ProduitsController@index');

app()->get('/produit/store', 'ProduitsController@store');

app()->get('/produit/show', 'ProduitsController@show');

app()->get('/produit/update', 'ProduitsController@update');

app()->get('/produit/destroy', 'ProduitsController@destroy');

// Les routes pour les endpoints des Produits dans les paniers

app()->get('/produitinpanier', 'ProduitInPaniersController@index');

app()->get('/produitinpanier/store', 'ProduitInPaniersController@store');

app()->get('/produitinpanier/show', 'ProduitInPaniersController@show');

app()->get('/produitinpanier/update', 'ProduitInPaniersController@update');

app()->get('/produitinpanier/destroy', 'ProduitInPaniersController@destroy');
