<?php

namespace App\Models;

class Personne extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    
    protected $table = 'personnes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'mail';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */

    protected $keyType = 'string';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    
    public $incrementing = false;

    /**
     * 
     */

    private static $table_name = 'breizhsport_schema.personnes';

    static function store($mail, $nom, $prenom, $password){

        db()->autoConnect();

        db()
        ->insert(self::$table_name)
        ->params([
          "mail" => $mail,
          "nom" => $nom,
          "prenom" => $prenom,
          "password" => $password
        ])
        ->execute();

        return self::show("");
    }

    static function show($mail){
        
        db()->autoConnect();
        
        if(strcmp($mail,"") !== 0){
            return db()->select(self::$table_name,"*")
                    ->where("mail",$mail)->all();
        }
        else{
            return db()->select(self::$table_name,"*")->all();
        }
        
    }

    static function updatePersonne($mail,$to_update){
        
        db()->autoConnect();
        
        db()
        ->update(self::$table_name)
        ->params($to_update)
        ->where("mail", $mail)
        ->execute();
    }

    static function destroy($mail){

        db()->autoConnect();

        Panier::destroyPanierPersonne($mail);

        db()->delete(self::$table_name)
            ->where("mail",$mail)->execute();
    }
    
}
