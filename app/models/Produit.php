<?php

namespace App\Models;

class Produit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    
     protected $table = 'produits';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'id_produit';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    
    public $incrementing = true;

    private static $table_name = 'breizhsport_schema.produits';

    
    static function store($nom, $description){

        db()->autoConnect();
        
        db()
        ->insert(self::$table_name)
        ->params([
          "nom" => $nom,
          "description" => $description
        ])
        ->execute();

        return db()->lastInsertId();
    }

    static function show($id_produit){
        
        db()->autoConnect();
        
        if(strcmp($id_produit,"") !== 0){
            return db()->select(self::$table_name,"*")
                    ->where("id_produit",$id_produit)->all();
        }
        else{
            return db()->select(self::$table_name,"*")->all();
        }
    }

    static function updateProduit( $id_produit,$to_update){
        
        db()->autoConnect();
        
        db()
        ->update(self::$table_name)
        ->params($to_update)
        ->where("id_produit",  $id_produit)
        ->execute();
    }

    static function destroy($id_produit){

        db()->autoConnect();

        ProduitInPanier::destroyProduitinPanier($id_produit,"");

        db()->delete(self::$table_name)
            ->where("id_produit", $id_produit)->execute();
    }
    
}
