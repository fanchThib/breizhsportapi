<?php

namespace App\Models;

class ProduitInPanier extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    
     protected $table = 'produit_dans_panier';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'id_panier';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    
    public $incrementing = false;

    private static $table_name = 'breizhsport_schema.produit_dans_panier';

    static function store($id_produit, $id_panier){

        db()->autoConnect();
        
        db()
        ->insert(self::$table_name)
        ->params([
          "id_produit" => $id_produit,
          "id_panier" => $id_panier
        ])
        ->execute();

        return db()->lastInsertId();
    }

    static function show($id_produit, $id_panier){
        
        db()->autoConnect();
        
        if((strcmp($id_produit,"") !== 0) || (strcmp($id_panier,"") !== 0)){
            return db()->select(self::$table_name,"*")
                    ->where(["id_produit"=>$id_produit,
                    "id_panier"=>$id_panier])->all();
        }
        else{
            return db()->select(self::$table_name,"*")->all();
        }
    }

    static function updateProduitinPanier( $id_produit,$id_panier,$to_update){
        
        db()->autoConnect();
        
        db()
        ->update(self::$table_name)
        ->params($to_update)
        ->where(["id_produit"=>$id_produit,
        "id_panier"=>$id_panier])
        ->execute();
    }

    static function destroyProduitinPanier($id_produit, $id_panier){

        db()->autoConnect();

        if((strcmp($id_produit,"") !== 0) && (strcmp($id_panier,"") == 0)){
            db()->delete(self::$table_name)
            ->where(["id_produit"=>$id_produit])
            ->execute();

            return "Produit supprimer car supprimer depuis Produit";
        }

        if((strcmp($id_produit,"") == 0) && (strcmp($id_panier,"") !== 0)){
            db()->delete(self::$table_name)
            ->where(["id_panier"=>$id_panier])
            ->execute();

            return "Produit supprimer car supprimer depuis Panier";
        }
        
        db()->delete(self::$table_name)
        ->where(["id_produit"=>$id_produit,
        "id_panier"=>$id_panier])
        ->execute();
    }

}
