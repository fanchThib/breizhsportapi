<?php

namespace App\Models;

class Panier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    
     protected $table = 'paniers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'id_panier';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    
    public $incrementing = false;

    private static $table_name = 'breizhsport_schema.paniers';


    static function store($mail_personne){

        db()->autoConnect();
        
        db()
        ->insert(self::$table_name)
        ->params([
          "mail_personne" => $mail_personne
        ])
        ->execute();

        return db()->lastInsertId();
    }

    static function show($id_panier){
        
        db()->autoConnect();
        
        if(strcmp($id_panier,"") !== 0){
            return db()->select(self::$table_name,"*")
                    ->where("id_panier",$id_panier)->all();
        }
        else{
            return db()->select(self::$table_name,"*")->all();
        }
    }

    static function showPanierPersonne($mail_personne){
        
        db()->autoConnect();
        
        return db()->select(self::$table_name,"*")
                    ->where("mail_personne",$mail_personne)->all();
        
    }

    static function updatePanier($id_panier,$to_update){
        
        db()->autoConnect();
        
        db()
        ->update(self::$table_name)
        ->params($to_update)
        ->where("id_panier",  $id_panier)
        ->execute();
    }

    static function destroy($id_panier){

        db()->autoConnect();

        ProduitInPanier::destroyProduitinPanier("",$id_panier);

        db()->delete(self::$table_name)
            ->where("id_panier", $id_panier)->execute();
    }

    static function destroyPanierPersonne($mail_personne){

        db()->autoConnect();

        $listPanierPersonne = Panier::showPanierPersonne($mail_personne);

        foreach ($listPanierPersonne as $key => $value) {
            //ProduitInPanier::destroyProduitinPanier("",$id_panier);
            print_r($listPanierPersonne[$key]);
        }
        
        db()->delete(self::$table_name)
            ->where("mail_personne", $mail_personne)->execute();
    }
    
}
