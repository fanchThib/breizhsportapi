<?php

namespace App\Controllers;

use App\Models\Produit;

class ProduitsController extends Controller
{    
    /**
     * Display a listing of the resource.
     */
    public function index() {

        $produits = Produit::show("");

        response()->json($produits);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $produit = Produit::store($_GET['nom'], $_GET['description']);
        
        response()->json($produit);

    }

    /**
     * Display the specified resource.
     */
    public function show()
    {

        $Produit = Produit::show($_GET['id_produit']);
        
        response()->json($Produit);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {

        $id_produit = $_GET['id_produit'];
        $to_update = array_filter($_GET, function($k) {
                                    return $k !== 'id_produit';
                                    }, ARRAY_FILTER_USE_KEY);

        Produit::updateProduit($id_produit, $to_update);

        response()->json($to_update);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {

        Produit::destroy($_GET['id_produit']);

        response()->json(['message' => ' produits Controller Destroy Function']);

    }
}
