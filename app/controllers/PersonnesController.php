<?php

namespace App\Controllers;

use App\Models\Personne;

class PersonnesController extends Controller
{    
    /**
     * Display a listing of the resource.
     */
    public function index() {

        $personnes = Personne::show("");

        response()->json($personnes);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $personne = Personne::store($_GET["mail"], $_GET['nom'], $_GET['prenom'], $_GET['password']);
        
        response()->json($personne);

    }

    /**
     * Display the specified resource.
     */
    public function show()
    {

        $personne = Personne::show($_GET['mail']);
        
        response()->json($personne);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {

        $mail = $_GET['mail'];
        $to_update = array_filter($_GET, function($k) {
                                    return $k !== 'mail';
                                    }, ARRAY_FILTER_USE_KEY);

        Personne::updatePersonne($mail, $to_update);

        response()->json(['message' => ' Personnes Controller Update Function']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {

        Personne::destroy($_GET['mail']);

        response()->json(['message' => ' Personnes Controller Destroy Function']);

    }

}
