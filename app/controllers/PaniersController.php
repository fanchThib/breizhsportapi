<?php

namespace App\Controllers;

use App\Models\Panier;

class PaniersController extends Controller
{    
    /**
     * Display a listing of the resource.
     */
    public function index() {

        $produits = Panier::show("");

        response()->json($produits);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $produit = Panier::store($_GET['mail_personne']);
        
        response()->json($produit);

    }

    /**
     * Display the specified resource.
     */
    public function show()
    {

        $Produit = Panier::show($_GET['id_panier']);
        
        response()->json($Produit);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {

        $id_panier = $_GET['id_panier'];
        $to_update = array_filter($_GET, function($k) {
                                    return $k !== 'id_panier';
                                    }, ARRAY_FILTER_USE_KEY);

        Panier::updatePanier($id_panier, $to_update);

        response()->json($to_update);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {

        Panier::destroy($_GET['id_panier']);

        response()->json(['message' => ' produits Controller Destroy Function']);

    }
}
