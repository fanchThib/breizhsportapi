<?php

namespace App\Controllers;

use App\Models\ProduitInPanier;

class ProduitInPaniersController extends Controller
{    
      
    /**
     * Display a listing of the resource.
     */
    public function index() {

        $produits = ProduitInPanier::show("","");

        response()->json($produits);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $produit = ProduitInPanier::store($_GET['id_produit'], $_GET['id_panier']);
        
        response()->json($produit);

    }

    /**
     * Display the specified resource.
     */
    public function show()
    {

        $Produit = ProduitInPanier::show($_GET['id_produit'],$_GET['id_panier']);
        
        response()->json($Produit);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {

        $id_produit = $_GET['id_produit'];
        $id_panier = $_GET['id_panier'];

        $to_update = array_filter($_GET, function($k) {
                                    return ($k !== 'id_produit') && ($k !== 'id_panier');
                                    }, ARRAY_FILTER_USE_KEY);

        ProduitInPanier::updateProduitinPanier($id_produit, $id_panier, $to_update);

        response()->json($to_update);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {

        ProduitInPanier::destroyProduitinPanier($_GET['id_produit'], $_GET['id_panier']);

        response()->json(['message' => ' produits Controller Destroy Function']);

    }
}
